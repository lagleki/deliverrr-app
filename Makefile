## Format all files on applications
format-all:
	nx format --all

## Lint all files on applications
lint-all:
	nx run-many --target=lint --all --only-failed

## Build all applications
build-all:
	nx run-many --target=build --all

## Bootstrap architect
bootstrap-up:
	docker network create deliverrr-network || true
	docker compose -f devops/docker-compose.local.yml --project-name deliverrr up --build -d
bootstrap-down:
	docker compose -f devops/docker-compose.local.yml down
crud-build:
	docker build -t deliverrr-crud-application:dev -f apps/deliverrr/devops/Dockerfile .
crud-up:
	docker compose -f apps/deliverrr/devops/docker-compose.local.yml --project-name deliverrr up --build -d
crud-down:
	docker compose -f apps/deliverrr/devops/docker-compose.dev.yml  --env-file ./.env  --project-name deliverrr down
