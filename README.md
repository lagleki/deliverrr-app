
# deliverrr app

## Functionality

provides API for deliveries

## Running locally for development

```
make bootstrap-up
make crud-up
```

  - it will create several docker containers. See if ports specified here are not taken:
  - devops/docker-compose.local.yml
  - apps/deliverrr/devops/docker-compose.local.yml

Then run locally the Node.js part (Node.js version 19 or later is preferable, e.g. use nvm):

```
yarn start deliverrr
```

## Build Node.js part for production

```
make crud-build
```

This also runs tests while building.

This doesn't take into account other containers necessary for production (MongoDB, Redis)

### API:

Now open Postman or analogs.

For all POST requests add the header
`Content-Type: application/json`

* Signup:

POST 127.0.0.1:3000/auth/signup

Body:
```
{
  "username": "john6",
  "password": "changeme",
  "type": 1
}
```
`type: 1` is for senders.
`type: 2` is for couriers.

* Login:

POST 127.0.0.1:3000/auth/login

Body:
```
{
  "username": "john6",
  "password": "changeme"
}
```

Will return a JWT token that should then be used for all authenticated endpoints as the header:

`Authorization: Bearer YOUR-TOKEN`

* Add personal data as a sender

If you registered as a sender

POST 127.0.0.1:3000/profile/addSenderData

```
{
  "companyName": "=891"
}
```

* Add personal data as a courier

If you registered as a courier

POST 127.0.0.1:3000/profile/addCourierData

```
{
  "phoneNumber": "=891"
}
```


* Fetch all your deliveries:

works both for couriers and senders:

POST 127.0.0.1:3000/delivery/all

Body:
```
{
  "skip": 0,
  "limit": 10
}
```

Will return the slice of deliveries + the total number of deliveries

* Create a new delivery:

Only works if you are a sender

POST 127.0.0.1:3000/delivery/add

```
{
  "description": "some luggage",
  "package_size": 5,
  "cost": 1,
  "date": "2022-12-05"
}
```

* Assign a delivery to a given courier:

You must know the ID of the courier (right now there is no possibility for senders to see a list of couriers to choose from, need more specs to implement):

Only works when the courier has up to MAX_DELIVERIES (environment variable) for the last 24 hours (by default 5).

Possible problems: 
- can reassign a delivery to another courier;
- can't remove any delivery from the courier
- can assign a delivery to yourself: no protection from that

POST 127.0.0.1:3000/delivery/assign

```
{
  "deliveryId": "638dae03d5f7bcb029c0ee32",
  "courier": "638da3dfdb5031315d9f42a3"
}
```

* Calculate your revenue

If you are a courier.

Right now calculated as the sum of costs of all your deliveries multiplied by REVENUE_SHARE env. variable (0.1 by default)

POST 127.0.0.1:3000/delivery/revenue
```
{
  "startDate": "2022-10-01",
  "endDate": "2022-12-06"
}
```

## Stack
- MongoDB - arbitrary choice
- Caching responses from common GET requests via Redis - not very useful currently
- Microservices functionality is mostly useless right now so disabled
- the db and the app are to be run on a shared network however, those jobs were excluded from the code
- Nginx is not included
- the server must have certain ports open to the world to provide access to controllers
- environment variables
  - for local deployment are specified locally
  - for dev/prod to be specified in CI/CD part on Gitlab etc.

## ToDo

- Swagger!
- typing is currently weak. DTO for responses is not implemented.
- more tests needed
- logging to MongoDB is currently not used
- move interceptors and schemas to <RootDir>/libs/
- repository.service should be split/moved into other services
- Some specs problems underlined above.
- Need to deploy to some (possibly) free hosting and test CI/CD with it
- Add ci/cd jobs for prod part
- Add microservices and move some heavy (e.g. statistics?) functionality there