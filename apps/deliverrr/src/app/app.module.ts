import { ConfigModule } from '@nestjs/config';
import { CacheModule, Module } from '@nestjs/common';
import * as redisStore from 'cache-manager-redis-store';
import type { ClientOpts as RedisClientOpts } from 'redis';
import { HttpModule } from '@nestjs/axios';
import { ClientsModule, RedisOptions, Transport } from '@nestjs/microservices';
import { ScheduleModule } from '@nestjs/schedule';
import { RepositoryService } from './repository.service';
import { HttpController } from './http.controller';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { DeliveriesModule } from './deliveries/deliveries.module';
import { DbModule } from './db/db.module';
import configuration from '../config/configuration';
const env = configuration();

@Module({
  imports: [
    DbModule,
    // ClientsModule.register([
    //   {
    //     name: 'MICROSERVICE',
    //     transport: Transport.TCP,
    //   },
    // ]),
    ScheduleModule.forRoot(),
    HttpModule,
    CacheModule.register<RedisClientOpts>({
      store: redisStore,
      host: env.cache.host,
      port: env.cache.port,
      ttl: 5, // seconds
      max: 10, // maximum number of items in cache
      isGlobal: true,
    }),
    ConfigModule.forRoot({ isGlobal: true, load: [configuration] }),
    AuthModule,
    UsersModule,
    DeliveriesModule,
  ],
  controllers: [HttpController],
  providers: [RepositoryService],
})
export class AppModule {}
