import { UserType } from '../../../enum/src';

export interface UserItem {
  id: string;
  username: string;
  type: UserType;
  password?: string;
}
