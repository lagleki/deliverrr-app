export interface RangeFilterOptions {
  startDate?: Date;
  endDate?: Date;
}
