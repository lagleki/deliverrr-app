export interface LogItem {
  _id: string;
  service: string;
  event?: string;
  level?: string;
  message?: string;
  createdAt?: Date;
}
