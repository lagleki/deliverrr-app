export interface FilterOptions {
  skip: number;
  limit: number;
}
