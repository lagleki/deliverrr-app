export interface CourierDataItem {
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  vehicleType?: string;
  refId: string;
}
