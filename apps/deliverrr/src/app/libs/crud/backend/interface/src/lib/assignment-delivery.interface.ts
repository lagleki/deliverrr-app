export interface AssignmentDeliveryItem {
  deliveryId: string;
  courier: string;
}
