export interface DeliveryItem {
  _id: string;
  package_size: number;
  cost: number;
  description: string;
  date: Date;
  sender?: string;
  courier?: string;
}
