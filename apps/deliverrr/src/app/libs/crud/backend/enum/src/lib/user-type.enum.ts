export enum UserType {
  'SENDER' = 1,
  'COURIER' = 2,
}
