import { IsOptional, IsString } from 'class-validator';
import { Expose, Type } from 'class-transformer';

export class CourierDataDto {
  @Expose()
  @IsString()
  @IsOptional()
  @Type(() => String)
  firstName?: string;
  
  @Expose()
  @IsString()
  @IsOptional()
  @Type(() => String)
  lastName?: string;
  
  @Expose()
  @IsString()
  @IsOptional()
  @Type(() => String)
  phoneNumber?: string;
  
  @Expose()
  @IsString()
  @IsOptional()
  @Type(() => String)
  vehicleType?: string;
}
