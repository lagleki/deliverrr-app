import { IsUUID } from 'class-validator';
import { Expose, Type } from 'class-transformer';

export class AssignDeliveryDto {
  @Expose()
  @Type(() => String)
  deliveryId: string;

  @Expose()
  @Type(() => String)
  courier: string;
}
