import { IsDate, IsOptional } from 'class-validator';
import { Expose, Type } from 'class-transformer';

export class ListRangeFilterDto {
  @Expose()
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  startDate?: Date;
  
  @Expose()
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  endDate?: Date;
}
