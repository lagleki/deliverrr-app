import { IsNumber, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';

export class AllUsersDto {
  @Expose()
  @IsOptional()
  @IsNumber()
  offset: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  limit: number;
}
