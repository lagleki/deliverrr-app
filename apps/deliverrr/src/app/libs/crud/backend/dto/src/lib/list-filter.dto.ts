import { IsDate, IsNumber, IsOptional } from 'class-validator';
import { Expose, Type } from 'class-transformer';

export class ListFilterDto {
  @Expose()
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  skip = 0;

  @Expose()
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  limit = 10;

  @Expose()
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  startDate?: Date;
  
  @Expose()
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  endDate?: Date;
}
