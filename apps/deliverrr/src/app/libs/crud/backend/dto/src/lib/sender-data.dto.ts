import { IsOptional, IsString } from 'class-validator';
import { Expose, Type } from 'class-transformer';

export class SenderDataDto {
  @Expose()
  @IsString()
  @IsOptional()
  @Type(() => String)
  companyName?: string;
}
