import { IsEnum, IsString } from 'class-validator';
import { Expose, Type } from 'class-transformer';
import { UserType } from '../../../enum/src';

export class CreateUserDto {
  @Expose()
  @IsString()
  @Type(() => String)
  username: string;
  
  @Expose()
  @IsString()
  @Type(() => String)
  password: string;

  @Expose()
  @IsEnum(UserType)
  type: UserType;
}
