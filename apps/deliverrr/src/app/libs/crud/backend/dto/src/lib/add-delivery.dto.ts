import { IsDate, IsNumber, IsOptional, IsString, IsUUID } from 'class-validator';
import { Expose, Type } from 'class-transformer';

export class AddDeliveryDto {
  @Expose()
  @IsNumber()
  @Type(() => Number)
  package_size: number;
  
  @Expose()
  @IsNumber()
  @Type(() => Number)
  cost: number;

  @Expose()
  @IsString()
  @Type(() => String)
  description: string;

  @Expose()
  @IsDate()
  @Type(() => Date)
  date: Date;

  @Expose()
  @IsUUID()
  @IsOptional()
  @Type(() => String)
  sender?: string;

  @Expose()
  @IsUUID()
  @IsOptional()
  @Type(() => String)
  courier?: string;
}
