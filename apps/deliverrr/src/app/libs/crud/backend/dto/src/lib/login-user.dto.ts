import { IsString } from 'class-validator';
import { Expose, Type } from 'class-transformer';

export class LoginUserDto {
  @Expose()
  @IsString()
  @Type(() => String)
  username: string;
  
  @Expose()
  @IsString()
  @Type(() => String)
  password: string;
}
