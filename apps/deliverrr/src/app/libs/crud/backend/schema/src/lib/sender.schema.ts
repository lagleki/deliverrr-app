import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as mongooseSchema } from 'mongoose';

@Schema({
  collection: 'sender',
  timestamps: true,
  autoIndex: true,
  versionKey: false,
})
export class SenderDataCollection extends Document {
  _id: string;

  @Prop({
    type: String,
    required: true,
    index: true,
  })
  companyName: string;

  @Prop({
    type: mongooseSchema.Types.ObjectId,
    required: true,
    index: true,
  })
  refId: string;
}

export const SenderDataSchema = SchemaFactory.createForClass(SenderDataCollection);

// //compound indices
// DeliverySchema.index({ });
