import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { UserType } from '../../../enum/src';

@Schema({
  collection: 'user',
  timestamps: true,
  autoIndex: true,
  versionKey: false,
})
export class UserCollection extends Document {
  _id: string;

  @Prop({
    type: String,
    required: true,
    index: true,
  })
  username: string;

  @Prop({
    type: String,
    required: true,
    index: true,
  })
  password: string;

  @Prop({
    type: Number,
    enum: UserType,
    default: UserType.SENDER,
    required: true,
    index: true,
  })
  type: UserType;
}

export const UserSchema = SchemaFactory.createForClass(UserCollection);

// //compound indices
// UserSchema.index({ username: 1 });
