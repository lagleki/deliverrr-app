import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as mongooseSchema } from 'mongoose';

@Schema({
  collection: 'delivery',
  timestamps: true,
  autoIndex: true,
  versionKey: false,
})
export class DeliveryCollection extends Document {
  _id: string;

  @Prop({
    type: Number,
    required: true,
    index: true,
  })
  package_size: number;

  @Prop({
    type: Number,
    required: true,
    index: true,
  })
  cost: number;

  @Prop({
    type: String,
    required: true,
    index: true,
  })
  description: string;

  @Prop({
    type: Date,
    required: true,
    index: true,
  })
  date: Date;

  @Prop({
    type: mongooseSchema.Types.ObjectId,
    required: false,
    index: true,
  })
  sender?: string;

  @Prop({
    type: mongooseSchema.Types.ObjectId,
    required: false,
    index: true,
  })
  courier?: string;
}

export const DeliverySchema = SchemaFactory.createForClass(DeliveryCollection);

// //compound indices
// DeliverySchema.index({ });
