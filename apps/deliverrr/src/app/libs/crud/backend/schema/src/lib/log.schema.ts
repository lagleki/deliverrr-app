import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
  collection: 'log',
  timestamps: true,
  autoIndex: false,
  versionKey: false,
})
export class LogCollection extends Document {
  _id: string;

  @Prop({ type: Date, index: true, expires: 86400 * 30 })
  createdAt: Date;

  @Prop({ type: String, required: true, index: false })
  service: string;

  @Prop({ type: String, index: false })
  event?: string;

  @Prop({ type: String, required: true, index: false })
  message?: string;

  @Prop({ type: String, index: false })
  level?: string;
}

export const LogSchema = SchemaFactory.createForClass(LogCollection);
