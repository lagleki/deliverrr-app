import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as mongooseSchema } from 'mongoose';

@Schema({
  collection: 'courier',
  timestamps: true,
  autoIndex: true,
  versionKey: false,
})
export class CourierDataCollection extends Document {
  _id: string;

  @Prop({
    type: String,
    required: false,
    index: true,
  })
  firstName?: string;

  @Prop({
    type: String,
    required: false,
    index: true,
  })
  lastName?: string;

  @Prop({
    type: String,
    required: false,
    index: true,
  })
  phoneNumber: string;

  @Prop({
    type: String,
    required: false,
    index: true,
  })
  vehicleType: string;

  @Prop({
    type: mongooseSchema.Types.ObjectId,
    required: true,
    index: true,
  })
  refId: string;
}

export const CourierDataSchema = SchemaFactory.createForClass(CourierDataCollection);

// //compound indices
// DeliverySchema.index({ });
