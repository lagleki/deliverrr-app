import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';

describe('AuthService', () => {
  let service: AuthService;
  let userService: UsersService;

  let module: TestingModule;
  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: '',
          signOptions: { expiresIn: '60m' },
        }),
        ConfigModule.forRoot({ isGlobal: true }),
      ],
      providers: [AuthService, UsersService, {
        provide: UsersService,
        useValue: {
          findOne: jest.fn(),
        }
      }],
    })
      .compile();

    service = module.get<AuthService>(AuthService);
    userService = module.get<UsersService>(UsersService);
  });

  it('should be defined', async () => {
    expect(service).toBeDefined();
  });

  it('login should work', async () => {
    const user = {
      id: '1',
      username: 'user',
      type: 1,
    };
    const password = '123';
    const hash = await bcrypt.hash(password, 10);

    jest
      .spyOn(userService, 'findOne')
      .mockImplementation(async () => ({ ...user, password: hash }));
    expect(await service.validateUser(user.username, password)).toEqual(user);
  });

  afterEach(async () => {
    await module.close();
  });
});
