import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UserItem } from '../libs/crud/backend/interface/src';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (!user) return null;
    const isMatch = await bcrypt.compare(pass, user.password);

    if (isMatch) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: Partial<UserItem>): Promise<{ access_token: string }> {
    const payload = { username: user.username, password: user.password };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async signup(
    user: Partial<UserItem>
  ): Promise<{ access_token: string } | null> {
    const existingUser: Partial<UserItem> | undefined = await this.usersService.findOne(
      user?.username
    );
    if (!existingUser) {
      const hash = await bcrypt.hash(
        user.password,
        this.configService.get('encryption.salt')
        );
      const newUser: Partial<UserItem> | undefined = await this.usersService.createOne({
        ...user,
        password: hash,
      });
      const payload = {
        username: newUser.username,
        password: newUser.password,
      };
      return {
        access_token: this.jwtService.sign(payload),
      };
    }
    return null;
  }
}
