import configuration from '../../config/configuration';
const env = configuration();

export const jwtConstants = {
	secret: env.jwt.secret,
  };