import { UserType } from './libs/crud/backend/enum/src';
import {
  CourierDataItem,
  LogItem,
  SenderDataItem,
  UserItem,
} from './libs/crud/backend/interface/src';
import {
  CourierDataCollection,
  LogCollection,
  SenderDataCollection,
  UserCollection,
} from './libs/crud/backend/schema/src';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable, throwError } from 'rxjs';
import {
  catchError,
  map,
} from 'rxjs/operators';
import { CustomLogger } from './utils/logger';
import { loggingTransports } from './utils/logLevels';

@Injectable()
export class RepositoryService {
  public readonly _mongoLogger: CustomLogger;

  constructor(
    @InjectModel(LogCollection.name, 'logDb')
    private readonly _logModel: Model<LogCollection>,
    @InjectModel(UserCollection.name, 'storageDb')
    private readonly _userModel: Model<UserCollection>,
    @InjectModel(CourierDataCollection.name, 'storageDb')
    private readonly _courierModel: Model<CourierDataCollection>,
    @InjectModel(SenderDataCollection.name, 'storageDb')
    private readonly _senderModel: Model<SenderDataCollection>
  ) {
    this._mongoLogger = new CustomLogger(
      this._logModel,
      'crud',
      loggingTransports
    );
  }

  /**
   * @description add a log record
   * @param {Partial<LogItem>} data object to log
   * @returns {boolean} always true
   */
  public createLogRecord(data: Partial<LogItem>): Observable<boolean> {
    return from(new this._logModel(data).save()).pipe(
      catchError((err) => {
        console[data?.level || 'log'](
          JSON.stringify({ method: 'createLogItem', error: err, data })
        );
        return throwError(() => new Error(err));
      }),
      map(() => true)
    );
  }

  public async createUser(
    user: Partial<UserItem>
  ): Promise<Partial<UserItem> | undefined> {
    return await new this._userModel(user).save();
  }

  public async updateOneCourier(
    user: Partial<UserItem>,
    data: Partial<CourierDataItem>
  ): Promise<boolean> {
    if (user.type !== UserType.COURIER)
      throw new BadRequestException({
        name: 'Validation error',
        message: ['Not a courier'],
      });
    const res = await this._courierModel
      .updateOne({ refId: user.id }, data, { upsert: true })
      .lean()
      .exec();
    return res?.modifiedCount === 1 || res?.upsertedCount === 1;
  }

  public async updateOneSender(
    user: Partial<UserItem>,
    data: Partial<SenderDataItem>
  ): Promise<boolean> {
    if (user.type !== UserType.SENDER)
      throw new BadRequestException({
        name: 'Validation error',
        message: ['Not a sender'],
      });
    const res = await this._senderModel
      .updateOne({ refId: user.id }, data, { upsert: true })
      .lean()
      .exec();
    return res?.modifiedCount === 1 || res?.upsertedCount === 1;
  }

  public async findUser(
    username: string
  ): Promise<Partial<UserItem> | undefined> {
    try {
      return this._userModel.findOne({ username }).lean().exec();
    } catch (error) {
      return undefined;
    }
  }
}
