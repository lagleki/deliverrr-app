import { LogLevel } from '@nestjs/common';

export const loggerLevels: LogLevel[] = [
  'log',
  'error',
  'warn',
  'verbose',
  'debug',
];

export const loggingTransports = { mongo: true, stdout: true };
