import { LogItem } from '../libs/crud/backend/interface/src';
import { LogCollection } from '../libs/crud/backend/schema/src';
import { Logger, LoggerService } from '@nestjs/common';
import { Model } from 'mongoose';
import { loggerLevels } from './logLevels';

interface ILogSettings {
  mongo: boolean;
  stdout: boolean;
}

export class CustomLogger implements LoggerService {
  private readonly standardLogger: Logger = new Logger('crud');
  private logModel: Model<LogCollection>;
  private service: string;
  private settings: ILogSettings;
  constructor(
    model: Model<LogCollection>,
    service: string,
    { mongo, stdout }: ILogSettings
  ) {
    this.logModel = model;
    this.service = service;
    this.settings = { mongo, stdout };
  }

  private convertMessage(message: string | Partial<LogItem>): Partial<LogItem> {
    if (typeof message === 'string') return { message };
    return message;
  }

  log(message: string | Partial<LogItem>, context?: string) {
    message = {
      event: context,
      ...this.convertMessage(message),
      service: this.service,
      level: 'log',
    };
    if (this.settings.mongo) new this.logModel(message).save();
    if (this.settings.stdout && loggerLevels.includes('log'))
      this.standardLogger.log(JSON.stringify(message), message.event);
  }

  error(message: string | Partial<LogItem>, context?: string) {
    message = {
      event: context,
      ...this.convertMessage(message),
      service: this.service,
      level: 'error',
    };
    if (this.settings.mongo) new this.logModel(message).save();
    if (this.settings.stdout && loggerLevels.includes('error'))
      this.standardLogger.error(JSON.stringify(message), null, message.event);
  }

  warn(message: string | Partial<LogItem>, context?: string) {
    message = {
      event: context,
      ...this.convertMessage(message),
      service: this.service,
      level: 'warn',
    };
    if (this.settings.mongo) new this.logModel(message).save();
    if (this.settings.stdout && loggerLevels.includes('warn'))
      this.standardLogger.warn(JSON.stringify(message), message.event);
  }

  debug?(message: string | Partial<LogItem>, context?: string) {
    message = {
      event: context,
      ...this.convertMessage(message),
      service: this.service,
      level: 'debug',
    };
    if (this.settings.mongo) new this.logModel(message).save();
    if (this.settings.stdout && loggerLevels.includes('debug'))
      this.standardLogger.debug(JSON.stringify(message), message.event);
  }

  verbose?(message: string | Partial<LogItem>, context?: string) {
    message = {
      event: context,
      ...this.convertMessage(message),
      service: this.service,
      level: 'verbose',
    };
    if (this.settings.mongo) new this.logModel(message).save();
    if (this.settings.stdout && loggerLevels.includes('verbose'))
      this.standardLogger.verbose(JSON.stringify(message), message.event);
  }
}
