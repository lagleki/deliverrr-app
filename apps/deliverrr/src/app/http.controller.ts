import {
  Body,
  CACHE_MANAGER,
  Controller,
  Get,
  Inject,
  Request,
  Post,
  UseGuards,
  CacheInterceptor,
  UseInterceptors,
} from '@nestjs/common';
import {
  AddDeliveryDto,
  AssignDeliveryDto,
  CourierDataDto,
  CreateUserDto,
  ListFilterDto,
  ListRangeFilterDto,
  LoginUserDto,
  SenderDataDto,
} from './libs/crud/backend/dto/src';

import { GetUser } from './decorators/user.decorator';
import { Cache } from 'cache-manager';
import { AuthService } from './auth/auth.service';
import { UsersService } from './users/users.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { DeliveriesService } from './deliveries/deliveries.service';

@Controller()
export class HttpController {
  constructor(
    @Inject(CACHE_MANAGER) private _cacheService: Cache,
    private authService: AuthService,
    private userService: UsersService,
    private deliveryService: DeliveriesService
  ) {}

  @Get('health')
  getHealth() {
    return 'ok';
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Body() body: LoginUserDto) {
    return this.authService.login(body);
  }

  @Post('auth/signup')
  async signup(@Body() body: CreateUserDto) {
    return this.authService.signup(body);
  }

  @UseGuards(JwtAuthGuard)
  @Post('profile/addCourierData')
  async addCourierData(@GetUser() user: any, @Body() body: CourierDataDto) {
    return this.userService.updateOneCourier(user, {...body, refId: user.id});
  }

  @UseGuards(JwtAuthGuard)
  @Post('profile/addSenderData')
  async addSenderData(@GetUser() user: any, @Body() body: SenderDataDto) {
    return this.userService.updateOneSender(user, {...body, refId: user.id});
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }

  @UseGuards(JwtAuthGuard)
  @Post('delivery/add')
  async addDelivery(@GetUser() user: any, @Body() delivery: AddDeliveryDto) {
    return this.deliveryService.addDelivery({ user, delivery });
  }

  @UseGuards(JwtAuthGuard)
  @Post('delivery/assign')
  async assignDelivery(@GetUser() user: any, @Body() delivery: AssignDeliveryDto) {
    return this.deliveryService.assignDelivery({ user, delivery });
  }
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(CacheInterceptor)
  @Post('delivery/all')
  async getDeliveries(@GetUser() user: any, @Body() options: ListFilterDto) {
    return this.deliveryService.listDeliveries({ user, options });
  }

  @UseGuards(JwtAuthGuard)
  @UseInterceptors(CacheInterceptor)
  @Post('delivery/revenue')
  async getDeliveriesRevenue(@GetUser() user: any, @Body() options: ListRangeFilterDto) {
    return this.deliveryService.listDeliveriesRevenue({ user, options });
  }
}
