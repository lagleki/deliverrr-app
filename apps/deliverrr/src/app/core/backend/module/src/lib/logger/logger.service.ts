import { LoggerService } from '@nestjs/common';

interface ILogSettings {
  mongo: boolean;
  stdout: boolean;
}

export class CustomLogger implements LoggerService {
  private app: string;
  private event: string;
  private settings: ILogSettings;
  private loggerLevels: string[];

  constructor(
    app: string,
    event: string,
    private readonly _repositoryService: any,
    { mongo, stdout }: ILogSettings,
    loggerLevels: string[]
  ) {
    this.app = app;
    this.event = event;
    this.settings = { mongo, stdout };
    this.loggerLevels = loggerLevels;
  }
  log(message: string, context?: string, moreFields = {}) {
    if (this.settings.mongo)
      this._repositoryService.createLogItem({
        app: this.app ?? 'crud-processor',
        event: context ?? this.event ?? 'processing',
        message,
        level: 'log',
        ...moreFields,
      });
    if (this.settings.stdout && this.loggerLevels.includes('log'))
      console.log(message);
  }

  error(message: string, context?: string, moreFields = {}) {
    if (this.settings.mongo)
      this._repositoryService.createLogItem({
        app: this.app ?? 'crud-processor',
        event: context ?? this.event ?? 'processing',
        message,
        level: 'error',
        ...moreFields,
      });
    if (this.settings.stdout && this.loggerLevels.includes('error'))
      console.log(message);
  }

  warn(message: string, context?: string, moreFields = {}) {
    if (this.settings.mongo)
      this._repositoryService.createLogItem({
        app: this.app ?? 'crud-processor',
        event: context ?? this.event ?? 'processing',
        message,
        level: 'warn',
        ...moreFields,
      });
    if (this.settings.stdout && this.loggerLevels.includes('warn'))
      console.log(message);
  }

  debug?(message: string, context?: string, moreFields = {}) {
    if (this.settings.mongo)
      this._repositoryService.createLogItem({
        app: this.app ?? 'crud-processor',
        event: context ?? this.event ?? 'processing',
        message,
        level: 'debug',
        ...moreFields,
      });
    if (this.settings.stdout && this.loggerLevels.includes('debug'))
      console.log(message);
  }

  verbose?(message: string, context?: string, moreFields = {}) {
    if (this.settings.mongo)
      this._repositoryService.createLogItem({
        app: this.app ?? 'crud-processor',
        event: context ?? this.event ?? 'processing',
        message,
        level: 'verbose',
        ...moreFields,
      });
    if (this.settings.stdout && this.loggerLevels.includes('verbose'))
      console.log(message);
  }
}
