import {
  CallHandler,
  ExecutionContext,
  Injectable,
  InternalServerErrorException,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  private readonly _logger: Logger = new Logger();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const executionStartTime = Date.now();
    const transportType = context.getType();
    let requestString = '';
    if (transportType === 'http') {
      const request = context.switchToHttp().getRequest();
      const method: 'GET' | 'POST' | 'PATCH' | 'DELETE' = request.method;
      if (method === 'GET') {
        const payload = new Object();
        Object.keys(request.query).forEach((e) => {
          payload[e] = request.query[e];
        });
        requestString = JSON.stringify(payload);
      } else if (method === 'PATCH') {
        const payload = new Object();
        Object.keys(request.query).forEach((e) => {
          payload[e] = request.query[e];
        });
        Object.assign(payload, { body: request.body });
        requestString = JSON.stringify(payload);
      } else {
        requestString = JSON.stringify(request.body);
      }
    } else {
      const request = context.switchToRpc().getData();
      requestString = JSON.stringify(request);
    }

    this._logger.log(`[Start] => ${requestString}`);
    return next.handle().pipe(
      tap(() => {
        const executionTime = Date.now() - executionStartTime;
        this._logger.log(`[Finish::${executionTime}ms] => ${requestString}`);
      }),
      catchError((err) => {
        const executionTime = Date.now() - executionStartTime;

        if (err instanceof Error) {
          if (err instanceof InternalServerErrorException) {
            this._logger.error(
              `[${err.message}::${executionTime}ms] => [${JSON.stringify(
                err
              )}] => ${requestString}`,
              null
            );
          } else {
            err.message = ((err as any)?.response?.message ??(err as any)?.message ?? [])
              .map((i: any) =>
                i.constraints
                  ? Object.keys(i.constraints ?? {})
                      .map((key) => i?.constraints?.[key])
                      .join(', ')
                  : i
              )
              .join('; ');
            this._logger.warn(
              `[${err.message}::${executionTime}ms] => ${requestString}}`
            );
          }
        } else {
          this._logger.error(JSON.stringify(err), err.stack.toString());
        }
        return throwError(err);
      })
    );
  }
}
