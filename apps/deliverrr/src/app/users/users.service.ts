import { CourierDataItem, SenderDataItem, UserItem } from '../libs/crud/backend/interface/src';
import { Injectable } from '@nestjs/common';
import { RepositoryService } from '../repository.service';

@Injectable()
export class UsersService {
  constructor(
    private readonly _repositoryService: RepositoryService,
  ) {}

  async findOne(username: string): Promise<Partial<UserItem> | undefined> {
    return this._repositoryService.findUser(username);
  }

  async createOne(user: Partial<UserItem>): Promise<Partial<UserItem> | undefined> {
    return this._repositoryService.createUser(user);
  }

  async updateOneCourier(user: Partial<UserItem>, data: Partial<CourierDataItem>): Promise<boolean> {
    return this._repositoryService.updateOneCourier(user, data);
  }

  async updateOneSender(user: Partial<UserItem>, data: Partial<SenderDataItem>): Promise<boolean> {
    return this._repositoryService.updateOneSender(user, data);
  }
}