import { Module } from '@nestjs/common';
import { DbModule } from '../db/db.module';
import { RepositoryService } from '../repository.service';
import { UsersService } from './users.service';

@Module({
  imports: [DbModule],
  providers: [UsersService, RepositoryService],
  exports: [UsersService],
})
export class UsersModule {}
