import { Test, TestingModule } from '@nestjs/testing';
import { DeliveriesService } from './deliveries.service';
import { ConfigModule } from '@nestjs/config';
import { DeliveryCollection } from '../libs/crud/backend/schema/src';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';

describe('DeliveriesService', () => {
  let service: DeliveriesService;
  let mockDeliveriesModel: Model<DeliveryCollection>;
  let module: TestingModule;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ isGlobal: true })],
      providers: [
        {
          provide: getModelToken(DeliveryCollection.name, 'storageDb'),
          useValue: Model,
        },
        DeliveriesService,
      ],
    }).compile();

    mockDeliveriesModel = module.get<Model<DeliveryCollection>>(
      getModelToken(DeliveryCollection.name,'storageDb')
    );
    service = module.get<DeliveriesService>(DeliveriesService);
  });

  it('should be defined', async () => {
    expect(service).toBeDefined();
  });
  it('should list deliveries', async () => {
    const user = {
      id: '638dc66b801b40f8ee50c0e8',
      username: 'user',
      type: 1,
    };

    jest
      .spyOn(mockDeliveriesModel, 'aggregate') // <- spy on what you want
      .mockResolvedValue([
        { paginatedResult: [], totalCount: { totalCount: 1 } },
      ]);
    expect(
      await service.listDeliveries({ user, options: { limit: 10, skip: 0 } })
    ).toEqual({ count: 0, deliveries: [], options: { limit: 10, skip: 0 } });
  });

  afterEach(async () => {
    await module.close();
  });
});
