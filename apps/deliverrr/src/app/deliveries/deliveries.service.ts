import {
  AssignmentDeliveryItem,
  DeliveryItem,
  FilterOptions,
  RangeFilterOptions,
  UserItem,
} from '../libs/crud/backend/interface/src';
import { DeliveryCollection } from '../libs/crud/backend/schema/src';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ObjectId } from 'mongodb';
import { UserType } from '../libs/crud/backend/enum/src';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DeliveriesService {
  private maxDeliveries: number;
  private revenueShare: number;
  constructor(
    @InjectModel(DeliveryCollection.name, 'storageDb')
    private readonly _deliveryModel: Model<DeliveryCollection>,
    private configService: ConfigService
  ) {
    this.maxDeliveries = this.configService.get('maxDeliveries');
    this.revenueShare = this.configService.get('revenueShare');
  }

  async listDeliveries({
    user,
    options,
  }: {
    user: UserItem;
    options: FilterOptions;
  }): Promise<{
    deliveries: Partial<DeliveryItem>[];
    count: number;
    options: FilterOptions;
  }> {
    const query = {
      [user.type === UserType.SENDER ? 'sender' : 'courier']: new ObjectId(
        user.id
      ),
    };

    const stats = await this._deliveryModel.aggregate([
      {
        $facet: {
          paginatedResult: [
            { $match: query },
            { $skip: options.skip },
            { $limit: options.limit },
          ],
          totalCount: [{ $match: query }, { $count: 'totalCount' }],
        },
      },
    ]);
    return {
      deliveries: stats?.[0]?.paginatedResult ?? [],
      count: stats?.[0]?.totalCount?.[0]?.totalCount ?? 0,
      options,
    };
  }

  async listDeliveriesRevenue({
    user,
    options,
  }: {
    user: UserItem;
    options: RangeFilterOptions;
  }): Promise<{
    revenue: number;
    options: RangeFilterOptions;
  }> {
    const query: any = {
      courier: new ObjectId(user.id),
    };
    if (options.startDate || options.endDate) query.date = {};
    if (options.startDate) query.date.$gte = new Date(options.startDate);
    if (options.endDate) query.date.$lte = new Date(options.endDate);

    const stats = await this._deliveryModel.aggregate([
      { $match: query },
      { $group: { _id: null, sum: { $sum: '$cost' } } },
    ]);
    return {
      revenue:
        Math.round((stats?.[0]?.sum ?? 0) * this.revenueShare * 100) / 100,
      options,
    };
  }

  async addDelivery({
    user,
    delivery,
  }: {
    user: UserItem;
    delivery: Partial<DeliveryItem>;
  }): Promise<DeliveryItem | undefined> {
    if (user.type !== UserType.SENDER)
      throw new BadRequestException({
        name: 'Validation error',
        message: ['Only senders can create deliveries'],
      });
    return await new this._deliveryModel({
      ...delivery,
      sender: new ObjectId(user.id),
    }).save();
  }

  private async _calcRecentDeliveries(
    courier: string,
    seconds = 86400
  ): Promise<number> {
    return this._deliveryModel
      .countDocuments({
        courier: new ObjectId(courier),
        date: { $gte: new Date(new Date().getTime() - seconds * 1000) },
      })
      .exec(); //if you are using promise
  }

  async assignDelivery({
    user,
    delivery,
  }: {
    user: UserItem;
    delivery: AssignmentDeliveryItem;
  }): Promise<boolean> {
    if (user.type !== UserType.SENDER)
      throw new BadRequestException({
        name: 'Validation error',
        message: ['Only senders can assign deliveries'],
      });
    const couriersLoad = await this._calcRecentDeliveries(
      delivery.courier,
      24 * 60 * 60
    );
    if (couriersLoad > this.maxDeliveries)
      throw new BadRequestException({
        name: 'Logistics error',
        message: ['Courier unavailable for deliveries'],
      });
    return (
      (
        await this._deliveryModel
          .updateOne(
            { _id: new ObjectId(delivery.deliveryId) },
            { ...delivery, sender: user.id }
          )
          .exec()
      )?.modifiedCount === 1
    );
  }
}
