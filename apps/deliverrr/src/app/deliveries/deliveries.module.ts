import { Module } from '@nestjs/common';
import { DbModule } from '../db/db.module';
import { RepositoryService } from '../repository.service';
import { DeliveriesService } from './deliveries.service';

@Module({
  imports: [DbModule],
  providers: [DeliveriesService, RepositoryService],
  exports: [DeliveriesService],
})
export class DeliveriesModule {}
