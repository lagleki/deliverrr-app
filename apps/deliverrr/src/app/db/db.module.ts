import {
  CourierDataCollection,
  CourierDataSchema,
  DeliveryCollection,
  DeliverySchema,
  LogCollection,
  LogSchema,
  SenderDataCollection,
  SenderDataSchema,
  UserCollection,
  UserSchema,
} from '../libs/crud/backend/schema/src';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import configuration from '../../config/configuration';
const env = configuration();

@Module({
  imports: [
    MongooseModule.forRoot(
      `mongodb://${env.storageDb.username}:${env.storageDb.password}@${env.storageDb.host}:${env.storageDb.port}/${env.storageDb.name}`,
      {
        useNewUrlParser: true,
        autoIndex: true,
        connectionName: 'storageDb',
      }
    ),
    MongooseModule.forFeature(
      [{ name: UserCollection.name, schema: UserSchema }],
      'storageDb'
    ),
    MongooseModule.forFeature(
      [{ name: CourierDataCollection.name, schema: CourierDataSchema }],
      'storageDb'
    ),
    MongooseModule.forFeature(
      [{ name: SenderDataCollection.name, schema: SenderDataSchema }],
      'storageDb'
    ),
    MongooseModule.forFeature(
      [{ name: DeliveryCollection.name, schema: DeliverySchema }],
      'storageDb'
    ),
    MongooseModule.forRoot(
      `mongodb://${env.logDb.username}:${env.logDb.password}@${env.logDb.host}:${env.logDb.port}/${env.logDb.name}`,
      {
        useNewUrlParser: true,
        autoIndex: true,
        connectionName: 'logDb',
      }
    ),
    MongooseModule.forFeature(
      [{ name: LogCollection.name, schema: LogSchema }],
      'logDb'
    ),
  ],
  providers: [],
  exports: [MongooseModule],
})
export class DbModule {}
