export default () => ({
  host: process.env.HOST || '127.0.0.1',
  port: parseInt(process.env.PORT, 10) || 80,
  jwt: { secret: process.env.JWT_SECRET || 'WdfgYDfrtT67eT2FD' },
  encryption: { salt: 10 },
  maxDeliveries: process.env.MAX_DELIVERIES || 5,
  revenueShare: process.env.REVENUE_SHARE || 0.1,
  production: process.env.PRODUCTION === 'true',
  logDb: {
    host: process.env.LOG_DB_HOST || '127.0.0.1',
    port: parseInt(process.env.LOG_DB_PORT, 10) || 8004,
    name: process.env.LOG_DB_NAME || 'admin',
    username: process.env.LOG_DB_USERNAME || 'admin',
    password: process.env.LOG_DB_PASSWORD || '12345678',
  },
  storageDb: {
    host: process.env.DB_HOST || '127.0.0.1',
    port: parseInt(process.env.DB_PORT, 10) || 8003,
    name: process.env.DB_NAME || 'admin',
    username: process.env.DB_USERNAME || 'admin',
    password: process.env.DB_PASSWORD || '12345678',
  },
  cache: {
    host: process.env.CACHE_HOST || 'deliverrr-crud-redis-dev',
    port: process.env.CACHE_PORT || 6379,
  }
});
