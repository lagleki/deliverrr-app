export default () => ({
  host: process.env.HOST || '0.0.0.0',
  port: parseInt(process.env.PORT, 10) || 3000,
  jwt: { secret: 'WdfgYT2FD' },
  encryption: { salt: 10 },
  maxDeliveries: process.env.MAX_DELIVERIES || 5,
  revenueShare: parseFloat(process.env.REVENUE_SHARE) || 0.1,
  production: process.env.PRODUCTION === 'true',
  storageDb: {
    host: '127.0.0.1',
    port: 8003,
    name: 'admin',
    username: 'admin',
    password: '12345678',
  },
  logDb: {
    host: '127.0.0.1',
    port: 8004,
    name: 'admin',
    username: 'admin',
    password: '12345678',
  },
  cache: {
    host: '127.0.0.1',
    port: 6379,
  },
});
