import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { LoggingInterceptor } from './app/core/backend/interceptor/src';
import { loggerLevels } from './app/utils/logLevels';
import {
  NestFastifyApplication,
} from '@nestjs/platform-fastify';

import configuration from './config/configuration';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { ValidationErrorFilter } from './app/exceptions/mongoose.exceptions.filter';
const env = configuration();

/** Catch process exceptions and exists */
process.on('exit', exitHandler.bind(null, { cleanup: true }));
process.on('SIGINT', exitHandler.bind(null, { exit: true }));
process.on('SIGUSR1', exitHandler.bind(null, { exit: true }));
process.on('SIGUSR2', exitHandler.bind(null, { exit: true }));
process.on('uncaughtException', exitHandler.bind(null, { exit: false }));

/** Bootstrap configuration */
async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    {
      logger: loggerLevels,
      cors: {
        origin: '*',
      },
    }
  );
  app.useGlobalFilters(new ValidationErrorFilter());
  app.useGlobalInterceptors(new LoggingInterceptor());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: {
        strategy: 'excludeAll',
        exposeUnsetFields: false,
      },
      // whitelist: true,
      // forbidNonWhitelisted: true,
      // forbidUnknownValues: true,
    })
  );
  // app.connectMicroservice<MicroserviceOptions>(
  //   {
  //     transport: Transport.TCP,
  //   },
  //   { inheritAppConfig: true }
  // );
  // await app.startAllMicroservices();
  await app.listen(env.port, env.host);
}

/** Catch errors */
function exitHandler(options, exitCode) {
  if (options.cleanup) {
    console.log('exit: clean');
  }
  if (exitCode || exitCode === 0) {
    if (exitCode !== 0) {
      console.error(exitCode, exitCode.stack);
    } else {
      console.log(`exit: code - ${exitCode}`);
    }
  }
  if (options.exit) {
    process.exit();
  }
}

bootstrap();
