module.exports = {
  roots: ['<rootDir>/'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  projects: ['<rootDir>/apps/deliverrr'],
};
